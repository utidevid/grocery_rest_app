<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <h1>Product</h1>
        </div>

        <div class="row" id="members">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name <a href="/products?sort=name_desc">↑</a><a href="/products?sort=name_asc">↓</a></th>
                    <th>Price <a href="/products?sort=price_desc">↑</a><a href="/products?sort=price_asc">↓</a></th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)

                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->calculateValidPrice() }}</td>
                            <td>
                                <form action="/">
                                    <input type="submit" value="Buy" class="btn btn-primary" />
                                    <input type="hidden" name="id" value="{{ $product->id }}" />
                                </form>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

<script>

    $(function() {
        $('#members').delegate('table tbody form', 'submit', function(e) {
            e.preventDefault();

            var form = $(this);

            var correctData = {};

            for (var i = 0; i < form.serializeArray().length; i++){
                correctData[form.serializeArray()[i]['name']] = form.serializeArray()[i]['value'];
            }

            $.ajax({
                url: '{{ $routes['buy_product'] }}', // url where to submit the request
                type : "PUT", // type of action POST || GET
                dataType : 'json', // data type
                data : JSON.stringify(correctData), // post data || get data
                success : function(result) {
                    form.parent().parent().fadeOut(300, function() { $(this).remove(); });
                },
                error: function(xhr, resp, text) {
                    console.log(xhr, resp, text);
                },
                contentType: 'application/json'
            });
        })
    });
</script>
</body>
</html>
