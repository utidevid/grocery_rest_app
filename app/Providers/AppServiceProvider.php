<?php

namespace App\Providers;

use App\Models\Entities\Product;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Custom validation rules

        // Start date should not be later than end date. Applies only to start date
        Validator::extend('earlier_than_end_date', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            return strtotime($value) <= strtotime($data['end_date']);
        });

        // End date should not be earlier than start date. Applies only to end date
        Validator::extend('later_than_start_date', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            return strtotime($value) >= strtotime($data['start_date']);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
