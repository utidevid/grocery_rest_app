<?php

namespace App\Models\Services;
use App\Models\Entities\Product;
use App\Models\Entities\Voucher;
use App\Models\Repositories\Product\ProductInterface;
use Illuminate\Http\Request;

/**
 * Product service that contains handy methods for business logic around Product
 *
 * Class ProductService
 * @package App\Models\Services
 */
class ProductService
{
    /**
     * Product repository to make database calls to
     *
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * Loads product repository associated with product interface
     *
     * ProductService constructor.
     * @param ProductInterface $productRepository
     */
    public function __construct(ProductInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Purchase a product
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function buy($id)
    {
        $product = $this->productRepository->getProductById($id);

        if ($product) {

            if ($product->status == Product::STATUS_PURCHASED) {
                return $product;
            }

            $product->status = Product::STATUS_PURCHASED;

            $product->save();

            // Update vouchers: after product is purchased, its vouchers are used
            $vouchers = $product->vouchers()->get();

            foreach ($vouchers as $voucher) {
                $voucher->status = Voucher::STATUS_USED;
                $voucher->save();
            }

            return $product;
        }

        return null;
    }

    /**
     * Buy a product
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        $product = $this->productRepository->getModel();

        $product->name = $request->json('name');
        $product->price = $request->json('price');
        $product->status = Product::STATUS_ACTIVE;

        $product->save();

        return $product;
    }

    /**
     * Get product data in JSON format by ID
     *
     * @param $id
     * @return null|string
     */
    public function getJsonDataById($id)
    {
        $product = $this->getDataById($id);

        if ($product) {
            return json_encode($product);
        }

        return null;
    }

    /**
     * Get product data by ID
     *
     * @param $id
     * @return mixed
     */
    public function getDataById($id)
    {
        $product = $this->productRepository->getProductArrayById($id);

        return $product;
    }
}