<?php

namespace App\Models\Services\Product;
use App\Models\Services\ProductService;
use Illuminate\Support\ServiceProvider;

/**
 * Register Product service with Lavarel
 *
 * Class ProductServiceServiceProvider
 * @package App\Models\Services
 */
class ProductServiceServiceProvider extends ServiceProvider
{
    /**
     * Register the service in the IoC container
     */
    public function register()
    {
        $this->app->bind('productService', function($app) {
            return new ProductService($app->make('App\Models\Repositories\Product\ProductInterface'));
        });
    }
}