<?php

namespace App\Models\Services;

use Illuminate\Support\Facades\Facade;

/**
 * Product facade that is triggered each time Product service is called
 *
 * Class ProductFacade
 * @package App\Models\Services
 */
class ProductFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'productService';
    }
}