<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    const VALIDATION_RULES = [
        'start_date' => 'required|date_format:Y-m-d|earlier_than_end_date',
        'end_date' => 'required|date_format:Y-m-d',
        'dt_id' => 'required|exists:discount_tiers,id'
    ];

    const STATUS_AVAILABLE = 1;
    const STATUS_USED = 2;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'vouchers';

    /**
     * The model is not timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Products that belong to the voucher
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * Discount tier that belongs to voucher
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discountTier()
    {
        return $this->hasOne('App\DiscountTier', 'id', 'dt_id');
    }

    /**
     * Is voucher valid
     *
     * @return bool
     */
    public function isValid()
    {
        return time() >= strtotime($this->start_date) && time() <= strtotime($this->end_date)
            && $this->status == self::STATUS_AVAILABLE;
    }
}
