<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class DiscountTier extends Model
{
    const MAX_DISCOUNT = 0.6;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'discount_tiers';

    /**
     * The model is not timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = ['discount'];
}
