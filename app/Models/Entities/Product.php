<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const VALIDATION_RULES = [
        'name' => 'required|max:100',
        'price' => 'required|numeric|min:0.01|max:99999999.99'
    ];

    // Active
    const STATUS_ACTIVE = 1;
    // Purchased
    const STATUS_PURCHASED = 2;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The model is not timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Calculate a valid product price
     *
     * @return float
     */
    public function calculateValidPrice()
    {
        $discounts = array();

        foreach ($this->vouchers()->get() as $voucher) {
            if ($voucher->isValid())
                $discounts[] = $voucher->discountTier()->getResults()->discount;
        }

        $discountTiersSum = array_sum($discounts);

        // Do not exceed maximum discount of 0.6
        if ($discountTiersSum > DiscountTier::MAX_DISCOUNT)
            $discountTiersSum = DiscountTier::MAX_DISCOUNT;

        return round($this->price - ($this->price * $discountTiersSum), 2);
    }

    /**
     * Vouchers that belong to the product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vouchers()
    {
        return $this->belongsToMany('App\Models\Entities\Voucher', 'product_voucher',
            'product_id', 'voucher_id');
    }
}
