<?php

namespace App\Models\Repositories\Product;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductRepository implements ProductInterface
{

    /**
     * Eloquent Product Model
     *
     * @var \App\Models\Entities\Product
     */
    protected $productModel;

    /**
     * Setting Product class to the injected model
     *
     * ProductRepository constructor.
     * @param Model $productModel
     */
    public function __construct(Model $productModel)
    {
        $this->productModel = $productModel;
    }

    /**
     * Get model
     *
     * @return \App\Models\Entities\Product|Model
     */
    public function getModel()
    {
        return $this->productModel;
    }


    /**
     * Get product data as array by ID
     *
     * @param $productId
     * @return null|object
     */
    public function getProductArrayById($productId)
    {
        return $this->convertFormat($this->productModel->find($productId));
    }


    /**
     * Converting the Eloquent object to a standard class
     *
     * @param $product
     * @return null|object
     */
    protected function convertFormat($product)
    {
        return $product ? (object) $product->toArray() : null;
    }

    /**
     * Get product model by ID
     *
     * @param $productId
     * @return Model
     */
    public function getProductById($productId)
    {
        return $this->productModel->find($productId);
    }

    /**
     * Get product vouchers
     *
     * @return BelongsToMany
     */
    public function getVouchers()
    {
        die(var_dump($this->productModel));
        return $this->productModel->vouchers();
    }
}