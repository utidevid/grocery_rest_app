<?php

namespace App\Models\Repositories\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface ProductInterface
{

    /**
     * Get model
     * @return Model
     */
    public function getModel();

    /**
     * Get product model by ID
     *
     * @param $productId
     * @return Model
     */
    public function getProductById($productId);

    /**
     * Get product data as array by ID
     *
     * @param $productId
     * @return array|null
     */
    public function getProductArrayById($productId);

    /**
     * Get product vouchers
     *
     * @return BelongsToMany
     */
    public function getVouchers();
}