<?php

namespace App\Models\Repositories\Product;

use App\Models\Entities\Product;
use Illuminate\Support\ServiceProvider;

/**
 * Register repository with Lavarel
 *
 * Class ProductRepositoryServiceProvider
 * @package App\Models\Repositories\Product
 */
class ProductRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the Product interface with Lavarel IoC container
     */
    public function register()
    {
        // Bind the returned class to the namespace 'Repositories\Product\ProductInterface'
        $this->app->bind('App\Models\Repositories\Product\ProductInterface', function($app) {
            return new ProductRepository(new Product());
        });
    }
}