<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use App\Models\Entities\Product;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Product::VALIDATION_RULES);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }

        $product = app('productService')->create($request);

        return response()->json(['data' => $product, 'msg' => 'Successfully created'], 201);
    }

    /**
     * Buy a product
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function buy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        $productId = $request->json('id');

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }

        $product = app('productService')->buy($productId);

        if ($product) {
            return response()->json(['data' => $product, 'msg' => 'Successfully purchased'], 200);
        }

        return response()->json(['errors' => ['No product found by ' . $productId . ' ID']], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Product $product
     */
    public function index()
    {
        $products = Product::all();

        if ($products->count() > 0) {
            return response()->json($products, 200);
        }

        return response()->json(['msg' => 'No products have been created yet'], 404);
    }

    /**
     * Show a certain product
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $product = app('productService')->getDataById($id);

        if ($product) {
            return response()->json($product, 200);
        }

        return response()->json(['errors' => ['No product found by ' . $id . ' ID']], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    /*public function edit(Product $product)
    {
        // Edit a product
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, Product $product)
    {
        $product->update($request->all());

        return response()->json($product, 200);
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    /*public function destroy(Product $product)
    {
        $product->delete();

        return response()->json(null, 204);
    }*/
}
