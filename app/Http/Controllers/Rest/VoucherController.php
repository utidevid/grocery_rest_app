<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use App\Models\Entities\Product;
use App\Models\Entities\Voucher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Validator;

class VoucherController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->isJson()) {
            return response()->json(['errors' => ['Only JSON content is permitted']], 400);
        }

        // Validate json body
        if (!$request->all()) {
            return response()->json(['errors' => ['JSON is invalid']], 400);
        }

        $validator = Validator::make($request->all(), Voucher::VALIDATION_RULES, [
            'start_date.earlier_than_end_date' => 'Start date should not be later than end date',
            'end_date.later_than_start_date' => 'End date should not be earlier than start date'
        ]);

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()->all()], 400);

        try {
            $voucher = new Voucher();

            $voucher->start_date = $request->json('start_date');
            $voucher->end_date = $request->json('end_date');
            $voucher->status = Voucher::STATUS_AVAILABLE;
            $voucher->dt_id = $request->json('dt_id');

            $voucher->save();

            return response()->json(['data' => $voucher, 'msg' => 'Successfully created'], 201);

        } catch (\Exception $e) {

            return response()->json(['msg' => 'Something went wrong'], 500);
        }
    }

    /**
     * Bind a voucher to a product
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function bind(Request $request)
    {
        if (!$request->isJson()) {
            return response()->json(['errors' => ['Only JSON content is permitted']], 400);
        }

        // Validate json body
        if (!$request->all()) {
            return response()->json(['errors' => ['JSON is invalid']], 400);
        }

        $validator = Validator::make($request->all(), [
            'pid' => 'required|exists:products,id',
            'vid' => 'required|exists:vouchers,id'
        ], [
            'vid.exists' => 'No voucher by the given ID was found',
            'pid.exists' => 'No product by the given ID was found'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }

        // Disallow binding if the product is already purchased
        $product = Product::find($request->json('pid'));

        if ($product->status == Product::STATUS_PURCHASED) {
            return response()->json(['errors' => ['The product that is being linked is already purchased']], 400);
        }

        try {
            $voucher = Voucher::find($request->json('vid'));
            $voucher->products()->attach($request->json('pid'));

            return response()->json(['msg' => 'Successfully linked'], 201);
        } catch (QueryException $queryException) {
            switch ($queryException->getCode()) {
                case 23000:
                    return response()->json(['errors' => 'Voucher is already linked to the given product'], 500);

                default:
                    return response()->json(['errors' => $queryException->getMessage()], 500);
            }
        } catch (\Exception $exception) {
            return response()->json(['msg' => 'Something went wrong'], 500);
        }
    }

    /**
     * Unbind a voucher from a product
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function unbind(Request $request)
    {
        if (!$request->isJson()) {
            return response()->json(['errors' => ['Only JSON content is permitted']], 400);
        }

        // Validate json body
        if (!$request->all()) {
            return response()->json(['errors' => ['JSON is invalid']], 400);
        }

        $validator = Validator::make($request->all(), [
            'pid' => 'required|exists:products,id',
            'vid' => 'required|exists:vouchers,id'
        ], [
            'vid.exists' => 'No voucher by the given ID was found',
            'pid.exists' => 'No product by the given ID was found'
        ]);


        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()->all()], 400);

        // Disallow binding if the product is already purchased
        $product = Product::find($request->json('pid'));

        if ($product->status == Product::STATUS_PURCHASED) {
            return response()->json(['errors' => ['The product that is being linked is already purchased']], 400);
        }

        try {
            $voucher = Voucher::find($request->json('vid'));
            $voucher->products()->detach($request->json('pid'));

            return response()->json(['msg' => 'Successfully unlinked'], 201);
        } catch (\Exception $exception) {
            return response()->json(['msg' => 'Something went wrong'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Product $product
     */
    public function index()
    {
        $vouchers = Voucher::all();

        if ($vouchers->count() > 0) {
            return response()->json($vouchers, 200);
        }

        return response()->json(['msg' => 'No vouchers have been created yet'], 404);
    }

    /**
     * Show a certain voucher
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $voucherId)
    {
        $voucherId = (int) $voucherId;

        if ($voucherId > 0) {
            try {
                $voucher = Voucher::findOrFail($voucherId);

                return response()->json($voucher, 200);

            } catch (ModelNotFoundException $e) {
                return response()->json(['errors' => ['No voucher with such ID was found']], 404);
            } catch (\Exception $e) {
                return response()->json(['msg' => 'Something went wrong'], 500);
            }
        }

        return response()->json(['errors' => ['Voucher ID is wrong']], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, Voucher $voucher)
    {
        // update a voucher
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    /*public function destroy(Voucher $voucher)
    {
        // delete a voucher
    }*/
}
