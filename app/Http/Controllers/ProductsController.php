<?php

namespace App\Http\Controllers;

use App\Models\Entities\Product;
use App\Models\Repositories\Product\ProductRepository;
use App\Models\Services\ProductService;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Render only active products
        $products = Product::where('status', Product::STATUS_ACTIVE);

        // Sort products by URL
        switch($request->get('sort', 'id')) {
            case 'id':
                $products->orderBy('id', 'asc');
                break;

            case 'name_asc':
                $products->orderBy('name', 'asc');
                break;

            case 'name_desc':
                $products->orderBy('name', 'desc');
                break;

            case 'price_asc':
                $products->orderBy('price', 'asc');
                break;

            case 'price_desc':
                $products->orderBy('price', 'desc');
                break;
        }

        return view('product_list',
            [
                'products' => $products->get(),
                'routes' => [
                    'buy_product' => redirect()-route('buy_product')
                ]
            ]
        );
    }
}
