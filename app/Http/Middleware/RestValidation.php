<?php

namespace App\Http\Middleware;

use Closure;

class RestValidation
{
    const AVAILABLE_REQUESTS = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
    ];

    const REQUESTS_WITH_JSON_CONTENT_TYPE = [
        'POST',
        'PUT',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // Check if request method is supported
        if (self::isRequestMethodSupported($request)) {
            // Check whether request method needs a json content-type
            if (self::doesRequestNeedJsonContentType($request)) {
                // Check if JSON content-type has been passed
                if ($request->isJson()) {
                    // Check if JSON is valid
                    if ($request->all()) {
                        return $next($request);
                    }

                    return response()->json(['errors' => ['JSON is invalid']], 400);
                }

                return response()->json(['errors' => ['JSON Content-type is required']], 400);
            }

            return $next($request);
        }

        return response()->json(['errors' => ['Request Method is not supported']], 405);
    }

    /**
     * Check if request method is supported
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    private static function isRequestMethodSupported($request)
    {
        return in_array($request->getMethod(), self::AVAILABLE_REQUESTS);
    }

    /**
     * Check if request needs needs JSON Content-type
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    private static function doesRequestNeedJsonContentType($request)
    {
        return in_array($request->getMethod(), self::REQUESTS_WITH_JSON_CONTENT_TYPE);
    }
}
