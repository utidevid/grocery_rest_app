<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Voucher::class, function (Faker\Generator $faker) {

    $dateRange = generateDates($faker);

    return [
        'start_date' => $dateRange['start_date'],
        'end_date' => $dateRange['end_date'],
        'status' => \App\Voucher::STATUS_AVAILABLE,
        'dt_id' => rand(1,4),
    ];
});

/**
 * Generate correct date range
 *
 * @param \Faker\Generator $faker
 * @return array
 */
function generateDates(Faker\Generator $faker)
{
    while (true) {
        $startDate = $faker->dateTimeBetween('-' . rand(1, 5) . ' days', '+' . rand(1, 5) . ' days')->format('Y-m-d');
        $endDate = $faker->dateTimeBetween('-' . rand(1, 5) . ' days', '+' . rand(1, 5) . ' days')->format('Y-m-d');

        if (strtotime($startDate) <= strtotime($endDate))
            return [
                'start_date' => $startDate,
                'end_date' => $endDate
            ];
    }
}


