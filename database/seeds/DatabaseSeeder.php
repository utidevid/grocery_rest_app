<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DiscountTiersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(VouchersTableSeeder::class);
        $this->call(ProductVoucherTableSeeder::class);
    }
}
