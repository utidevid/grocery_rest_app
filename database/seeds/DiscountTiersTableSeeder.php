<?php

use App\Models\Entities\DiscountTier;
use Illuminate\Database\Seeder;

class DiscountTiersTableSeeder extends Seeder
{
    const DISCOUNT_TIERS = array(0.1, 0.15, 0.2, 0.25);

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create required discount tiers
        foreach (self::DISCOUNT_TIERS as $tier) {
            DiscountTier::create(
                [
                    'discount'  =>  $tier,
                ]
            );
        }
    }
}
