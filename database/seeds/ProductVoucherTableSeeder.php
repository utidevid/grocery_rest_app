<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductVoucherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = DB::table('products')->get()->shuffle();

        foreach ($products as $product) {

            $vouchers = DB::table('vouchers')->get()->shuffle()->slice(0,rand(1,6))->all();


            foreach ($vouchers as $voucher) {
                DB::table('product_voucher')->insert(
                    [
                        'product_id' => $product->id,
                        'voucher_id' => $voucher->id,
                    ]
                );
            }
        }
    }
}
