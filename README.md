# README #

A REST API with a client that executes operations on vouchers and products. The data are being sent and retrieved in JSON format. Currently supports requests: creating a voucher with an associated discount tier, creating a product, buying a product, linking and unlinking a voucher on a product.

### Requirements ###

* Make sure the latest version of PHP is installed
* If you use Google Chrome, make sure it has a rest client installed. There are extensions available as Postman and Advanced Rest Client for Google Chrome. Other browsers should have appropriate extensions / plugins available, too

### Setup ###

* Clone the project to an executable directory of your php engine (git clone https://utidevid@bitbucket.org/utidevid/grocery_rest_app.git)
* Create a database called grocery
* Edit the database config file of the project according to your local settings. The file can be found in the config folder of the project
* Run the following commands: 
* `composer update` - to download dependencies
* `php artisan migrate` - to run database migrations
* `php artisan db:seed` - to seed the database with predefined values OR run `php artisan db:seed --class DiscountTierTableSeeder` to seed predefined discounts. Either should be executed
* `php artisan serve` - to start a local server to access the application client

### Usage ###

#### REST
* Create a product `curl -d '{"name":"Audi","price":30000}' -H "Content-Type: application/json" -X POST localhost:8000/api/product`
* Get a certain product `curl -X GET localhost:8000/api/product/1`
* Get all products `curl -X GET localhost:8000/api/products`
* Create a voucher `curl -d '{"start_date":"2017-07-10","end_date":"2017-07-11", "dt_id":1}' -H "Content-Type: application/json" -X POST localhost:8000/api/voucher`
* Create a certain voucher `curl -X GET localhost:8000/api/voucher/1`
* Create all vouchers `curl -X GET localhost:8000/api/vouchers`
* Bind a voucher to a product `curl -d '{"vid":2, "pid":1}' -H "Content-Type: application/json" -X POST localhost:8000/api/voucher/bind`
* Unbind a voucher from a product `curl -d '{"vid":2, "pid":1}' -H "Content-Type: application/json" -X POST localhost:8000/api/voucher/unbind`
* Buy a product `curl -d '{"id":1}' -H "Content-Type: application/json" -X PUT localhost:8000/api/product/buy`

#### Client
* To access the client, go to `localhost:8000/products`

### Clariications ###
1. Ideally, REST and Client should be separated into different modules (e.g. Symfony Bundles)
2. Request validity such as `checking if request has been sent with a JSON content type` could be moved out to a higher level, avoiding repeating the check in each controller.
3. Message generator and message outputter classes could be created to compose and render a message that API should return on each request
4. Update and delete for vouchers and products has not been implemented
5. Unit and integration test have not been implemented either


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Should you have any questions or seek further clarifications, please feel free to contact at andy.zaporozhets@gmail.com