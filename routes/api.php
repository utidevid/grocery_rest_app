<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/



// Products

// Create a product
Route::post('product', 'Rest\ProductController@store');

// Get products
//Route::get('product/{productId}', 'Rest\ProductController@show');
Route::get('product/{id}', 'Rest\ProductController@show')->where('id', '[0-9]+');
Route::get('products', 'Rest\ProductController@index');

// Buy a product
Route::put('product/buy', 'Rest\ProductController@buy')->name('buy_product');


// Vouchers

// Create a voucher
Route::post('voucher', 'Rest\VoucherController@store');

// Get vouchers
Route::get('voucher/{voucherId}', 'Rest\VoucherController@show');
Route::get('vouchers', 'Rest\VoucherController@index');

// Link and unlink vouchers
Route::post('voucher/bind', 'Rest\VoucherController@bind');
Route::post('voucher/unbind', 'Rest\VoucherController@unbind');
